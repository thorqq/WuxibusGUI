package com.thorqq.wuxibusgui;

public class Constants
{
    //AES加密相关
    public static String AESKey               = "1EXp6MhoWUNxPOIc";
    public static String IV                   = "0000000000000000";
    
    //网络超时
    public static int READ_TIMEOUT = 10;
    public static int WRITE_TIMEOUT = 10;
    public static int CONNECT_TIMEOUT = 10;

    //服务器URL
    public static String ServerUrl            = "http://api.wxbus.com.cn/api/";

    //服务器支持的功能
    public static class UrlParam
    {
        public static String GetStartPic = "get_start_pic";
        public static String Index = "index";
     
        public static String LineSearch = "line_search"; //根据关键字查找线路
        public static String LineSearchInfo = "line_search_info"; //根据line_title获取线路方向信息
        public static String LineStops = "line_stops";//根据line_id和direction（这个参数没用）获取所有方向的所有站台信息
        public static String BusLive = "bus_live";//根据line_code和stop_seq获取实时到站信息
    }


    public static int    AROUND_FLAG          = 1;
    public static String AdvertiseUrl         = "get_start_pic";
    public static String DeviceIMEI           = "";
    public static int    FAV_FLAG             = 0;
    public static int    Height;
    public static int    HomeLineSize         = 10;

    public static String Latitude             = "latitude";
    public static String Longitude            = "longitude";
    public static String LatitudeBaidu        = "latitude_baidu";
    public static String LongitudeBaidu       = "longitude_baidu";

    public static int    NetConnectionTimeOut = 15000;

    public static String RouteSearchUrl       = "/?m=line_search&k=";
    public static int    SEARCH_LINE_TYPE     = 0;
    public static int    SEARCH_PLACE_TYPE    = 2;
    public static int    SEARCH_STATIION_TYPE = 1;
    public static String SqliteName           = "line_search_history";
    public static int    Width;
    public static String charSet              = "UTF-8";

}
