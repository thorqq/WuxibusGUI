package com.thorqq.wuxibusgui;


public class ConfigMgr
{
    public static final String CONFIG_FILE = "cfg/Config.json";
    
    public static class Parameters
    {
        public int units = 3;
        public String keys[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "路", "线", "专线", "快"};       
        public String lines[] ={"131路", "138路", "快6", "快3"};
    }
    
    private static ConfigMgr mInstance = new ConfigMgr();
    private Config mConfig = new Config();
    private Parameters mParams = null;
    
    public static ConfigMgr getInstance()
    {
        return mInstance;
    }
    
    public void read()
    {
    	mParams = mConfig.read(CONFIG_FILE, Parameters.class);
    }
    
    public boolean save()
    {
    	return mConfig.write(mParams);
    }
    
    public Parameters getCfg()
    {
    	return mParams;
    }
    
}
