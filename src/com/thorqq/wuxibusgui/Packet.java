package com.thorqq.wuxibusgui;

import java.util.List;

public class Packet
{
	public static class Error
	{
		public static int SUCCESS = 1;
	}
	
	public static class CommonResp
	{
    	public static String ERROR = "error";
    	public static String RESULT = "result";
	}
	
    // ************************************************************
    //
    // 线路关键词搜索
    //
    // ************************************************************
	//{"k":"11","m":"line_search","nonce":"dc7f3257-7077-4e24-91fc-cbbf546053f8"}
	//{"error":1,"result":["11路","111路","112路","113路","115路","116路","118路","211路","311路","611路","711路","711支","711区","711区停"]}
    public static class LineSearchReq
    {
        public String k;
        public String m = Constants.UrlParam.LineSearch;
        public String nonce;
    }

    public static class LineSearchResp
    {
    	public List<String> result;
    }
    // ************************************************************
    //
    // 线路信息
    //
    // ************************************************************
    public static class LineDirectionInfo
    {
        public String line_id;
        public String line_title;
        public String line_code;
        public String line_name;
        public int direction;
        public String stop_start;
        public String stop_end;
    }
    
    // {"k":"11路","m":"line_search_info","nonce":"6d887281-6df3-4039-97d2-ac11ba4cb49f"}
    public static class LineSearchInfoReq
    {
        public String k;
        public String m = Constants.UrlParam.LineSearchInfo;
        public String nonce;
    }

	//{"k":"11路","m":"line_search_info","nonce":"6d887281-6df3-4039-97d2-ac11ba4cb49f"}
	//{"error":1,
	// "result":
	//  [
	//    {"line_id":"11","line_title":"11路","line_code":"33450414","line_name":"11环行","direction":0,"stop_start":"中南分公司（扬名公交总站）","stop_end":"无锡中央车站"},
	//    {"line_id":"11","line_title":"11路","line_code":"33450414","line_name":"11环行","direction":1,"stop_start":"无锡中央车站","stop_end":"中南分公司（扬名公交总站）"}
	//  ]
	//}
    public static class LineSearchInfoResp
    {
        public int                      error;
        public List<LineDirectionInfo> result;
    }

    // ************************************************************
    //
    // 线路站点
    //
    // ************************************************************
    public static class StopInfo
    {
        public int    line_id;        // "line_id": "11",
        public String line_code;      // "line_code": "33450414",
        public String stop_id;        // "stop_id": "55110519084315214018",
        public String stop_name;      // "stop_name": "中南分公司（扬名公交总站）",
        public int    stop_type_id;   // "stop_type_id": "6",
        public String stop_type_name; // "stop_type_name": "主站上客站",
        public int    stop_seq;       // "stop_seq": "1",
        public double longitude;      // "longitude": "120.311404",
        public double latitude;       // "latitude": "31.522913",
        public double longitude_baidu; // "longitude_baidu": "120.32256510354",
        public double latitude_baidu; // "latitude_baidu": "31.526784501226"
    }

    public static class LineDirectionStops
    {
        public int            line_id;       // "line_id": "11",
        public String         line_title;    // "line_title": "11路",
        public String         line_code;     // "line_code": "33450414",
        public String         line_name;     // "line_name": "11环行",
        public int            direction;     // "direction": 0,
        public String         stop_start;    // "stop_start": "中南分公司（扬名公交总站）",
        public String         stop_end;      // "stop_end": "无锡中央车站"
        public String         time_start_end; // "time_start_end": "05:00-23:30",
        public List<StopInfo> stops;
    }

    // {"direction":"0","line_id":"11","m":"line_stops","nonce":"c71d786d-cd58-4ae7-812c-91bcff9328e9"}
    public static class LineStopsReq
    {
        public String direction;
        public String line_id;
        public String m = Constants.UrlParam.LineStops;
        public String nonce;
    }

    public static class LineStopsResp
    {
        public int                      error;
        public List<LineDirectionStops> result;
    }

    // ************************************************************
    //
    // 实时到站
    //
    // ************************************************************
    public static class BusLiveStopInfo
    {
        public int    stop_limit;
        public String info;

        @Override
        public String toString()
        {
            return "BusLiveStopInfo [stop_limit=" + stop_limit + ", info=" + info + "]";
        }
    }

    public static class BusLiveStop
    {
        public String stop_name;  // "stop_name": "中联新村",
        public String actdatetime; // "actdatetime": "21:00:19",
        public int    stop_seq;   // "stop_seq": 6,
        public int    busselfid;  // "busselfid": "31864",
        public String productid;  // "productid": "51031864",
        public String lastBus;    // "lastBus": "0",
        public String pic;        // "pic": ""

        @Override
        public String toString()
        {
            return "BusLiveStop [stop_name=" + stop_name + ", actdatetime=" + actdatetime + ", stop_seq=" + stop_seq + ", busselfid=" + busselfid
                    + ", productid=" + productid + ", lastBus=" + lastBus + ", pic=" + pic + "]";
        }
    }

    public static class BusLive
    {
        public BusLiveStopInfo   stop_info;
        public String info;
        public List<BusLiveStop> list;

        @Override
        public String toString()
        {
            return "BusLive [stop_info=" + stop_info + ", list=" + list + "]";
        }
    }
    
    // {"line_code":"25647843","m":"bus_live","nonce":"cd792c12-c40e-450e-822a-c4ec1dcdb970","stop_seq":"31"}
    public static class BusLiveReq
    {
        public String line_code;
        public String stop_seq;
        public String m = Constants.UrlParam.BusLive;
        public String nonce;
    }

    public static class BusLiveResp
    {
        public int               error;
        public BusLiveStopInfo   stop_info;
        public String info;
        public List<BusLiveStop> list;

        @Override
        public String toString()
        {
            return "BusLive [error=" + error + ", stop_info=" + stop_info + ", list=" + list + "]";
        }
    }
}
