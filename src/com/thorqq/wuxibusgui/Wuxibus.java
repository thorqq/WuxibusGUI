package com.thorqq.wuxibusgui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;

public class Wuxibus
{

	protected Shell	mShell;
	private List<WuxibusUnit> mWuxibusUnitList = new ArrayList<WuxibusUnit>();
	private List<String> mAllLines = new ArrayList<String>();

	
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args)
	{
		Log.init();
		ConfigMgr.getInstance().read();
		
		try
		{
			Wuxibus window = new Wuxibus();
			window.open();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open()
	{
		Display display = Display.getDefault();
		createContents();
		mShell.open();
		mShell.layout();
		mShell.setMaximized(true);
		
		while (!mShell.isDisposed())
		{
			if (!display.readAndDispatch())
			{
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents()
	{
		mShell = new Shell();
		mShell.setSize(725, 537);
		mShell.setText("无锡公交实时查询");
		mShell.setLayout(new FormLayout());
		
		searchAllLine();
		
		for(int i = 0; i < ConfigMgr.getInstance().getCfg().units; i++)
		{
			if(i == 0)
			{
				WuxibusUnit unit = new WuxibusUnit(mShell, SWT.NONE);
				FormData fd_groupBusUnit = new FormData();
				fd_groupBusUnit.top = new FormAttachment(0, 10);
				fd_groupBusUnit.left = new FormAttachment(0, 10);
				fd_groupBusUnit.right = new FormAttachment(0, 353);
				fd_groupBusUnit.bottom = new FormAttachment(100, -10);
				unit.setLayoutData(fd_groupBusUnit);
				
				unit.setAllLines(mAllLines);
				mWuxibusUnitList.add(unit);
				
				if(i < ConfigMgr.getInstance().getCfg().lines.length)
				{
					unit.setDefaltLine(ConfigMgr.getInstance().getCfg().lines[i]);
				}
			}
			else
			{
				int pre = mWuxibusUnitList.size() - 1;
				WuxibusUnit unit = new WuxibusUnit(mShell, SWT.NONE);
				FormData fd_groupBusUnit = new FormData();
				fd_groupBusUnit.top = new FormAttachment(mWuxibusUnitList.get(pre), 0, SWT.TOP);
				fd_groupBusUnit.left = new FormAttachment(mWuxibusUnitList.get(pre), 0);
				fd_groupBusUnit.bottom = new FormAttachment(mWuxibusUnitList.get(pre), 0, SWT.BOTTOM);
				unit.setLayoutData(fd_groupBusUnit);
				
				unit.setAllLines(mAllLines);
				mWuxibusUnitList.add(unit);
				
				if(i < ConfigMgr.getInstance().getCfg().lines.length)
				{
					unit.setDefaltLine(ConfigMgr.getInstance().getCfg().lines[i]);
				}
			}
		}
		

	}
	
	protected void searchAllLine()
	{
		String[] arrayKeys = ConfigMgr.getInstance().getCfg().keys;
		Set<String> allLines = new HashSet<String>();
		for(String key : arrayKeys)
		{
			try
			{
				allLines.addAll(BusUtils.searchLine(key));
			}
			catch (Exception e)
			{
				Log.ex(e);
				e.printStackTrace();
			}
		}
		
		mAllLines.clear();
		mAllLines.addAll(allLines);
		Collections.sort(mAllLines);		
	}
	
}
