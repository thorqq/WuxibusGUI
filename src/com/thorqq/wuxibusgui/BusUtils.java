package com.thorqq.wuxibusgui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.thorqq.wuxibusgui.Packet.LineDirectionInfo;
import com.thorqq.wuxibusgui.Packet.LineDirectionStops;

public class BusUtils
{
//    public static void main(String[] argv)
//    {
//        Log.init();
//        Log.i("Wuxibus start");
//        
//        Main main = new Main();
//        try
//        {
//	        main.run();
//        }
//        catch (Exception e)
//        {
//	        e.printStackTrace();
//        }
//    }
//    
//    public void run() throws Exception
//    {
//        //根据关键词查找线路
//        List<String> allLines = this.searchLine("11");
//        
//        //根据线路名称获取线路信息
//        String line_title = allLines.get(0);
//        List<LineDirectionInfo> lineInfo = this.searchLineInfo(line_title);
//        
//        //根据line_id获取所有站台
//        List<LineDirectionStops> lineStops = this.getLineStops(lineInfo.get(0).line_id);
//        
//        //查询到站信息
//        List<BusLiveStop> busLiveStop = this.getBusLive(lineInfo.get(0).line_code);
//
//        
//    }
    
    /**
     * 根据关键词查找线路
     * @param key 关键词
     * @return 所有包含关键词的线路
     * @throws Exception 
     */
    @SuppressWarnings("unchecked")
    static public List<String> searchLine(String key) throws Exception
    {
    	//{"k":"1","m":"all_search","nonce":"fb2286dc-c1a1-4c6b-ae1d-63f4a8579c5b"}
    	//{"line":["1路","10路","11路","12路","13路"],"stop":["101医院","阳光100","312国道（城南路）","312国道（会岸路）","312国道（钱皋路）"],"place":[]}

    	List<String> result = new ArrayList<String>();
    	
    	//根据关键词查找线路
        Packet.LineSearchReq lineSearchReqPkg = new Packet.LineSearchReq();
        lineSearchReqPkg.k = key;
        String json = Net.request(lineSearchReqPkg);
        Log.i(Utils.formatJson(json));
        
        Map<String, Object> resp = JSON.parseObject(json, new TypeReference<Map<String, Object>>(){});
        if((Integer)resp.get(Packet.CommonResp.ERROR) != Packet.Error.SUCCESS)
        {
        	String errorInfo = (String)resp.get(Packet.CommonResp.RESULT);
        	Log.w(errorInfo);
        	throw new Exception(errorInfo);
        }

    	result = (List<String>)resp.get(Packet.CommonResp.RESULT);
        
        return result;
    }
    
    /**
     * 根据line_title获取线路方向信息
     * @param lineTitle
     */
    static public List<LineDirectionInfo> searchLineInfo(String lineTitle) throws Exception
    {
    	//{"k":"11路","m":"line_search_info","nonce":"6d887281-6df3-4039-97d2-ac11ba4cb49f"}
    	//{"error":1,
    	// "result":
    	//  [
    	//    {"line_id":"11","line_title":"11路","line_code":"33450414","line_name":"11环行","direction":0,"stop_start":"中南分公司（扬名公交总站）","stop_end":"无锡中央车站"},
    	//    {"line_id":"11","line_title":"11路","line_code":"33450414","line_name":"11环行","direction":1,"stop_start":"无锡中央车站","stop_end":"中南分公司（扬名公交总站）"}
    	//  ]
    	//}

        Packet.LineSearchInfoReq lineInfoReqPkg = new Packet.LineSearchInfoReq();
        lineInfoReqPkg.k = lineTitle;
        String json = Net.request(lineInfoReqPkg);
        Log.i(Utils.formatJson(json));

        Map<String, Object> resp = JSON.parseObject(json, new TypeReference<Map<String, Object>>(){});
        if(resp.get(Packet.CommonResp.ERROR) != null && (Integer)resp.get(Packet.CommonResp.ERROR) != Packet.Error.SUCCESS)
        {
        	String errorInfo = (String)resp.get(Packet.CommonResp.RESULT);
        	Log.w(errorInfo);
        	throw new Exception(errorInfo);
        }

        Packet.LineSearchInfoResp lineSearchInfo = JSON.parseObject(json, Packet.LineSearchInfoResp.class);
        
        return lineSearchInfo.result;
    }
    
    static public List<LineDirectionStops> getLineStops(String lineId) throws Exception
    {
        //站台
        Packet.LineStopsReq lineStopsReqPkg = new Packet.LineStopsReq();
        
        lineStopsReqPkg.line_id = lineId;
        //lineStopsReqPkg.direction = "0";这个参数没啥用
        String json = Net.request(lineStopsReqPkg);
        
        Map<String, Object> resp = JSON.parseObject(json, new TypeReference<Map<String, Object>>(){});
        if(resp.get(Packet.CommonResp.ERROR) != null && (Integer)resp.get(Packet.CommonResp.ERROR) != Packet.Error.SUCCESS)
        {
        	String errorInfo = (String)resp.get(Packet.CommonResp.RESULT);
        	Log.w(errorInfo);
        	throw new Exception(errorInfo);
        }
        
        Log.i(Utils.formatJson(json));

        Packet.LineStopsResp lineStops = JSON.parseObject(json, Packet.LineStopsResp.class);

        return lineStops.result;
    }
    
    static public Packet.BusLive getBusLive(String lineCode) throws Exception
    {
    	//实时到站
        Packet.BusLiveReq busLiveReqPkg = new Packet.BusLiveReq();
        busLiveReqPkg.line_code = lineCode;
        //busLiveReqPkg.stop_seq = "7"; //如果加了这个参数，就是提示距离该站还有几站，没啥用
        
        String json = Net.request(busLiveReqPkg);
        //Packet.BusLive busLive = Utils.fromJson(json, Packet.BusLive.class);

        Map<String, Object> resp = JSON.parseObject(json, new TypeReference<Map<String, Object>>(){});
        if(resp.get(Packet.CommonResp.ERROR) != null && (Integer)resp.get(Packet.CommonResp.ERROR) != Packet.Error.SUCCESS)
        {
        	String errorInfo = (String)resp.get(Packet.CommonResp.RESULT);
        	Log.w(errorInfo);
        	throw new Exception(errorInfo);
        }

        Log.i(Utils.formatJson(json));
        
        Packet.BusLiveResp busLiveResp = JSON.parseObject(json, Packet.BusLiveResp.class);

        Packet.BusLive busLive = new Packet.BusLive();
        busLive.info = busLiveResp.info;
        busLive.list = busLiveResp.list;
        
        return busLive;
    }
}
