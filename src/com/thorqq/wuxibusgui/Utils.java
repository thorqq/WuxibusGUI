package com.thorqq.wuxibusgui;

import java.util.UUID;

import sun.misc.BASE64Encoder;

public class Utils
{
    //随机串
    public static String getRandomUUID()
    {
        return UUID.randomUUID().toString();
    }
    
    //AES加密
    private static AES mAES = new AES();
    
    public static byte[] AESEncode(byte[] src)
    {
        return mAES.encrypt(src, Constants.AESKey.getBytes());
    }
    
    //Base64加密
    private static BASE64Encoder mBASE64Encoder = new BASE64Encoder();
    
    public static String Base64Encode(byte[] src)
    {
        return mBASE64Encoder.encode(src);
    }
    
    //Json格式化
    public static String formatJson(String jsonStr)
    {
        int level = 0;
        StringBuffer jsonForMatStr = new StringBuffer();
        for (int i = 0; i < jsonStr.length(); i++)
        {
            char c = jsonStr.charAt(i);
            if (level > 0 && '\n' == jsonForMatStr.charAt(jsonForMatStr.length() - 1))
            {
                jsonForMatStr.append(getLevelStr(level));
            }
            switch (c)
            {
            case '{':
            case '[':
                jsonForMatStr.append(c + "\n");
                level++;
                break;
            case ',':
                jsonForMatStr.append(c + "\n");
                break;
            case '}':
            case ']':
                jsonForMatStr.append("\n");
                level--;
                jsonForMatStr.append(getLevelStr(level));
                jsonForMatStr.append(c);
                break;
            default:
                jsonForMatStr.append(c);
                break;
            }
        }

        return jsonForMatStr.toString();

    }

    public static String getLevelStr(int level)
    {
        StringBuffer levelStr = new StringBuffer();
        for (int levelI = 0; levelI < level; levelI++)
        {
            levelStr.append("\t");
        }
        return levelStr.toString();
    }
}
